# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2022 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Structure of uniquely named and typed members data store and transform."""

from typing import Any, Dict, Optional

from .._default import NO_DEFAULT
from .._typing import FactoryFormat, Format
from ..array import ArrayX
from ..data import Data, DataMeta

def bitfield_member(
    doc: str = "",
    *,
    size: int,
    lsb: Optional[int] = None,
    typ: type = int,
    signed: bool = False,
    default: Any = NO_DEFAULT,
    ignore: bool = False,
    readonly: bool = False,
    compute: bool = False,
    nbytes: int = 0,
    argrepr: Optional[str] = None,
) -> Any:
    """Bit field structure member definition.

    :param doc: accessor documentation string
    :param size: bit field width (in bits)
    :param lsb: least significant bit position
    :param signed: interpret as signed integer
    :param typ: field type (e.g. ``IntEnum`` subclass)
    :param default: initializer default value
    :param ignore: ignore member during comparisons
    :param readonly: block setting member attribute
    :param compute: initializer defaults to compute based on another member
    :param nbytes: bytes occupied by this bit field and ones that follow (0 => calculate)
    :param argrepr: format to represent member argument in structure repr

    """

def dimmed_member(
    doc: str = "",
    *,
    fmt: ArrayX,
    dims: Any,  # Member
    default: Any = NO_DEFAULT,
    ignore: bool = False,
    readonly: bool = False,
    argrepr: Optional[str] = None,
) -> Any:
    """Define variably dimensioned array structure member.

    :param doc: accessor documentation string
    :param fmt: array transform (or factory function)
    :param dims: array dimensions member definition
    :param default: initializer default value
    :param ignore: ignore member during comparisons
    :param readonly: block setting member attribute
    :param argrepr: format to represent member argument in structure repr

    """

def member(
    doc: str = "",
    *,
    default: Any = NO_DEFAULT,
    ignore: bool = False,
    readonly: bool = False,
    compute: bool = False,
    argrepr: Optional[str] = None,
    fmt: FactoryFormat,
    fmt_arg: Any = None,  # Optional["Member"]
) -> Any:
    """Define structure member properties.

    :param doc: accessor documentation string
    :param default: initializer default value
    :param ignore: ignore member during comparisons
    :param readonly: block setting member attribute
    :param compute: initializer defaults to compute based on another member
    :param argrepr: format to represent member argument in structure repr
    :param fmt: member format
    :param fmt_arg: member property to use as format factory argument

    """

def sized_member(
    doc: str = "",
    *,
    fmt: Format,
    size: Any,  # must be Member, checked at runtime
    ratio: float = 1,
    offset: int = 0,
    default: Any = NO_DEFAULT,
    ignore: bool = False,
    readonly: bool = False,
    argrepr: Optional[str] = None,
) -> Any:
    """Sized structure member definition.

    :param doc: accessor documentation string
    :param fmt: member format, default uses type annotation
    :param size: member property which controls size
    :param int ratio: number of bytes per increment of size member
    :param int offset: difference in size (in bytes)
    :param default: initializer default value
    :param ignore: ignore member during comparisons
    :param readonly: block setting member attribute
    :param argrepr: format to represent member argument in structure repr

    """

class StructureMeta(DataMeta):

    """Structure data store metaclass."""

    @property
    def byteorder(cls) -> str:
        """Byte order ("little" or "big")."""

class Structure(Data, metaclass=StructureMeta):

    """Structured data store type."""

    def asdict(self) -> Dict[str, Any]:
        """Return structure members in dictionary form.

        :returns: structure members

        """

__all__ = [
    "bitfield_member",
    "dimmed_member",
    "member",
    "StructureMeta",
    "sized_member",
    "Structure",
]
