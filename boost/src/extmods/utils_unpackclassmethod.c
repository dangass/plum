// included in utils.c

/* unpack class method object */

/* A unpack class method receives the class as implicit first argument,
   just like an instance method receives the instance.
   To declare a unpack class method, use this idiom:

     class C:
         @PackMethod
         def __pack__(cls, arg1, arg2, ...):
             ...

*/

typedef struct {
    PyObject_HEAD
    PyObject *ucm_callable;
    PyObject *ucm_dict;
} unpackclassmethod;

static void
ucm_dealloc(unpackclassmethod *ucm)
{
    // FUTURE - is this needed?
    // _PyObject_GC_UNTRACK((PyObject *)ucm);
    Py_XDECREF(ucm->ucm_callable);
    Py_XDECREF(ucm->ucm_dict);
    Py_TYPE(ucm)->tp_free((PyObject *)ucm);
}

static int
ucm_traverse(unpackclassmethod *ucm, visitproc visit, void *arg)
{
    Py_VISIT(ucm->ucm_callable);
    Py_VISIT(ucm->ucm_dict);
    return 0;
}

static int
ucm_clear(unpackclassmethod *ucm)
{
    Py_CLEAR(ucm->ucm_callable);
    Py_CLEAR(ucm->ucm_dict);
    return 0;
}


static PyObject *
ucm_descr_get(PyObject *self, PyObject *obj, PyObject *type)
{
    unpackclassmethod *ucm = (unpackclassmethod *)self;

    if (ucm->ucm_callable == NULL) {
        PyErr_SetString(PyExc_RuntimeError,
                        "uninitialized unpackclassmethod object");
        return NULL;
    }
    if (type == NULL)
        type = (PyObject *)(Py_TYPE(obj));
    return PyMethod_New(self, type);
}

static PyObject *
ucm_call(PyObject *self, PyObject *args, PyObject *kwds)
{
    unpackclassmethod *ucm = (unpackclassmethod *)self;

    tracef("DUNDERUNPACKMETHOD_CALL");

    PyObject *cls;
    PyObject *buffer;
    PyObject *offset;
    PyObject *parent;
    PyObject *dump;
    PyObject *result;

    static char *kwlist[] = {"cls", "buffer", "offset", "parent", "dump", NULL};
    if (! PyArg_ParseTupleAndKeywords(args, kwds, "OOOOO", kwlist, &cls, &buffer, &offset, &parent, &dump)) {
        tracef("  DUNDERUNPACKMETHOD_CALL:KEYWORDS_FAIL");
        return NULL;
    }

    if (dump == Py_None) {
        Py_ssize_t final_offset = 0;
        PyObject *value = unpack_from_buffer(cls, buffer, offset, 0, parent, &final_offset);
        result = Py_BuildValue("(On)", value, final_offset);
    }
    else {
        result = PyObject_CallFunction(ucm->ucm_callable, "OOOOO", cls, buffer, offset, parent, dump);
    }

    return result;
}

static int
ucm_init(PyObject *self, PyObject *args, PyObject *kwds)
{
    unpackclassmethod *ucm = (unpackclassmethod *)self;
    PyObject *callable;

    if (!_PyArg_NoKeywords("unpackclassmethod", kwds))
        return -1;
    if (!PyArg_UnpackTuple(args, "unpackclassmethod", 1, 1, &callable))
        return -1;
    Py_INCREF(callable);
    Py_XSETREF(ucm->ucm_callable, callable);
    return 0;
}

static PyMemberDef ucm_memberlist[] = {
    {"__func__", T_OBJECT, offsetof(unpackclassmethod, ucm_callable), READONLY},
    {NULL}  /* Sentinel */
};

static PyObject *
ucm_get___isabstractmethod__(unpackclassmethod *ucm, void *closure)
{
    int res = _PyObject_IsAbstract(ucm->ucm_callable);
    if (res == -1) {
        return NULL;
    }
    else if (res) {
        Py_RETURN_TRUE;
    }
    Py_RETURN_FALSE;
}

static PyGetSetDef ucm_getsetlist[] = {
    {"__isabstractmethod__",
     (getter)ucm_get___isabstractmethod__, NULL,
     NULL,
     NULL},
    {"__dict__", PyObject_GenericGetDict, PyObject_GenericSetDict, NULL, NULL},
    {NULL} /* Sentinel */
};

PyDoc_STRVAR(unpackclassmethod_doc,
"unpackclassmethod(function) -> method\n\
\n\
Convert a function to be a unpack class method.\n\
\n\
A unpack class method receives the class as implicit first argument,\n\
just like an instance method receives the instance.\n\
To declare a unpack class method, use this idiom:\n\
\n\
  class C:\n\
      @unpackclassmethod\n\
      def f(cls, arg1, arg2, ...):\n\
          ...\n\
\n\
It can be called either on the class (e.g. C.f()) or on an instance\n\
(e.g. C().f()).  The instance is ignored except for its class.\n\
If a class method is called for a derived class, the derived class\n\
object is passed as the implied first argument.");

PyTypeObject UnpackClassMethod_Type = {
    PyVarObject_HEAD_INIT(&PyType_Type, 0)
    .tp_name = "unpackclassmethod",
    .tp_basicsize = sizeof(unpackclassmethod),
    .tp_dealloc = (destructor)ucm_dealloc,
    .tp_call = (ternaryfunc)ucm_call,
    .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE | Py_TPFLAGS_HAVE_GC,
    .tp_doc = unpackclassmethod_doc,
    .tp_traverse = (traverseproc)ucm_traverse,
    .tp_clear = (inquiry)ucm_clear,
    .tp_members = ucm_memberlist,
    .tp_getset = ucm_getsetlist,
    .tp_descr_get = ucm_descr_get,
    .tp_dictoffset = offsetof(unpackclassmethod, ucm_dict),
    .tp_init = ucm_init,
    .tp_alloc = PyType_GenericAlloc,
    .tp_new = PyType_GenericNew,
    .tp_free = PyObject_GC_Del,
};
