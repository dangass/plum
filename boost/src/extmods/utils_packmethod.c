// included in utils.c

typedef struct {
    PyObject_HEAD
    /* Type-specific fields go here. */
    PyObject *args;
} PackMethodObject;

static PyTypeObject PackMethodType;

static int PackMethod_init(PackMethodObject *self, PyObject *args, PyObject *kwds)
{
    tracef("PACKMETHOD_INIT");

    self->args = args;
    Py_INCREF(args);

    return 0;
}

static void PackMethod_dealloc(PackMethodObject *self)
{
    Py_XDECREF(self->args);
}

static PyObject * PackMethod_get(PackMethodObject *self, PyObject *obj, PyObject *type) {
    tracef("PACKMETHOD_GET");

    PyObject *args;

    if (obj) {
        tracef("  PACKMETHOD_GET:INSTANCE");
        args = PyTuple_Pack(2, type, obj);
    }
    else {
        tracef("  PACKMETHOD_GET:CLASS");
        args = PyTuple_Pack(1, type);
    }

    if (!args) {
        return NULL;
    }

    PyObject * method = PyObject_CallObject((PyObject*)&PackMethodType, args);
    Py_DECREF(args);

    return method;
};

static PyObject * PackMethod_call(PackMethodObject *self, PyObject *args, PyObject *kwds) {
    tracef("PACKMETHOD_CALL");

    PyObject *combined_args = PySequence_Concat(self->args, args);

    if (!combined_args) {
        return NULL;
    }

    PyObject *result = pack(NULL, combined_args, kwds);
    Py_DECREF(combined_args);

    return result;
}

static PyTypeObject PackMethodType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "PackMethod",
    .tp_doc = packmethod_docstring,
    .tp_basicsize = sizeof(PackMethodObject),
    .tp_itemsize = 0,
    .tp_dealloc = (destructor) PackMethod_dealloc,
    .tp_flags = Py_TPFLAGS_DEFAULT,
    .tp_new = PyType_GenericNew,
    .tp_init = (initproc) PackMethod_init,
    .tp_descr_get = (descrgetfunc) PackMethod_get,
    .tp_call = (ternaryfunc) PackMethod_call,
};
