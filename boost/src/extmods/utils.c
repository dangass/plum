#include <Python.h>
#include "structmember.h"
#include "fast.h"
#include "utils_docstrings.c"

PyObject *PyErr_ExcessMemoryError;
PyObject *PyErr_ImplementationError;
PyObject *PyErr_InsufficientMemoryError;
PyObject *plum_unpack_and_dump;
PyObject *plumunpack_from_buffer_and_dump;
PyObject *plum_pack_and_dump;
PyTypeObject *Plum_Type;
PyTypeObject *PlumMeta_Type;
PyTypeObject *PlumView_Type;
PyObject *fmt_string;
Py_hash_t fmt_hash;
PyObject *zero;

PyObject *async_method(PyObject *self) {
    PyErr_Format(
        PyExc_TypeError,
        "'%s' does not support async methods", Py_TYPE(self)->tp_name);

    return NULL;
}

inline FastStruct * get_faststruct(PyTypeObject *cls) {
    if ((cls->tp_as_async) && (cls->tp_as_async->am_anext == async_method)) {
        return (FastStruct*) cls->tp_as_async;
    }
    return NULL;
}


#include "utils_pack.c"
#include "utils_getbytes.c"
#include "utils_unpack.c"
#include "utils_fastutils.c"
#include "utils_packmethod.c"
#include "utils_packclassmethod.c"
#include "utils_unpackclassmethod.c"


static PyMethodDef ExtModMethods[] = {
    {"c_api_get_fastutils_pointer", c_api_get_fastutils_pointer, METH_NOARGS,
     ""},
    {"pack", (PyCFunction)pack, METH_VARARGS|METH_KEYWORDS,
     pack_docstring},
    {"unpack", (PyCFunction)unpack, METH_VARARGS,
     unpack_docstring},
    {"unpack_from", (PyCFunction)unpack_from, METH_VARARGS|METH_KEYWORDS,
     unpack_from_docstring},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};


static PyModuleDef utils_module = {
    PyModuleDef_HEAD_INIT,
    .m_name = "plum_boost._utils",
    .m_doc = "Pack/Unpack Memory utility functions.",
    .m_size = -1,
    .m_methods = ExtModMethods,
};

PyMODINIT_FUNC PyInit__utils(void) {
    fmt_string = NULL;
    zero = NULL;

    if (PyType_Ready(&PackMethodType) < 0)
        return NULL;

    PackClassMethod_Type.tp_base = &PyClassMethod_Type;
    if (PyType_Ready(&PackClassMethod_Type) < 0)
        return NULL;

    UnpackClassMethod_Type.tp_base = &PyClassMethod_Type;
    if (PyType_Ready(&UnpackClassMethod_Type) < 0)
        return NULL;

    Py_INCREF(&PackMethodType);
    Py_INCREF(&PackClassMethod_Type);
    Py_INCREF(&UnpackClassMethod_Type);

    fmt_string = PyUnicode_FromStringAndSize("fmt", 3);
    if (!fmt_string) {
       return NULL;
    }

    zero = PyLong_FromLong(0);
    if (!zero) {
        goto error;
    }


    fmt_hash = PyObject_Hash(fmt_string);
    if (fmt_hash == -1) {
        goto error;
    }

    /*
    from plum.utilities import unpack as plum_unpack_and_dump
    */

    PyObject *plum_module = PyImport_ImportModule("plum.utilities"); /* issued */
    if (plum_module == NULL) {
        goto error;
    }

    PyObject *plum_module_dict = PyModule_GetDict(plum_module); /* borrowed */
    Py_XDECREF(plum_module);
    if (plum_module_dict == NULL) {
        goto error;
    }

    Plum_Type = (PyTypeObject*) PyDict_GetItemString(plum_module_dict, "Plum"); /* borrowed */
    if (Plum_Type == NULL) {
        goto error;
    }

    PlumView_Type = (PyTypeObject*) PyDict_GetItemString(plum_module_dict, "PlumView"); /* borrowed */
    if (PlumView_Type == NULL) {
        goto error;
    }

    PlumMeta_Type = (PyTypeObject*) PyDict_GetItemString(plum_module_dict, "PlumMeta"); /* borrowed */
    if (PlumMeta_Type == NULL) {
        goto error;
    }

    plum_pack_and_dump = PyDict_GetItemString(plum_module_dict, "pack_and_dump"); /* borrowed */
    if (plum_pack_and_dump == NULL) {
        goto error;
    }

    plum_unpack_and_dump = PyDict_GetItemString(plum_module_dict, "unpack_and_dump"); /* borrowed */
    if (plum_unpack_and_dump == NULL) {
        goto error;
    }

    plumunpack_from_buffer_and_dump = PyDict_GetItemString(plum_module_dict, "unpack_from_and_dump"); /* borrowed */
    if (plum_unpack_and_dump == NULL) {
        goto error;
    }

    /*
    from plum.exceptions import ImplementationError
    */

    PyObject *exc_module = PyImport_ImportModule("plum.exceptions"); /* issued */
    if (exc_module == NULL) {
        goto error;
    }

    PyObject *exc_module_dict = PyModule_GetDict(exc_module); /* borrowed */
    Py_XDECREF(exc_module);
    if (exc_module_dict == NULL) {
        goto error;
    }

    PyErr_ExcessMemoryError = PyDict_GetItemString(exc_module_dict, "ExcessMemoryError"); /* borrowed */
    if (PyErr_ExcessMemoryError == NULL) {
        goto error;
    }

    PyErr_ImplementationError = PyDict_GetItemString(exc_module_dict, "ImplementationError"); /* borrowed */
    if (PyErr_ImplementationError == NULL) {
        goto error;
    }

    PyErr_InsufficientMemoryError = PyDict_GetItemString(exc_module_dict, "InsufficientMemoryError"); /* borrowed */
    if (PyErr_InsufficientMemoryError == NULL) {
        goto error;
    }

    /*
    Create module.
    */

    PyObject *this_module = PyModule_Create(&utils_module); /* issued (give to caller) */
    if (this_module != NULL) {
        if (PyModule_AddObject(this_module, "pack_classmethod", (PyObject *) &PackClassMethod_Type) < 0) {
            Py_DECREF(this_module);
            goto error;
        }
        if (PyModule_AddObject(this_module, "unpack_classmethod", (PyObject *) &UnpackClassMethod_Type) < 0) {
            Py_DECREF(this_module);
            goto error;
        }
        if (PyModule_AddObject(this_module, "PackMethod", (PyObject *) &PackMethodType) < 0) {
            Py_DECREF(this_module);
            goto error;
        }
        Py_INCREF(this_module); /* never allow this module to be deleted */
        Py_INCREF(Plum_Type);
        Py_INCREF(PlumMeta_Type);
        Py_INCREF(PlumView_Type);
        Py_INCREF(plum_pack_and_dump);
        Py_INCREF(plum_unpack_and_dump);
        Py_INCREF(plumunpack_from_buffer_and_dump);
        Py_INCREF(PyErr_ExcessMemoryError);
        Py_INCREF(PyErr_ImplementationError);
        Py_INCREF(PyErr_InsufficientMemoryError);
    }

    return this_module;

error:
    Py_DECREF(&PackMethodType);
    Py_DECREF(&PackClassMethod_Type);
    Py_DECREF(&UnpackClassMethod_Type);
    Py_XDECREF(fmt_string);
    Py_XDECREF(zero);

    return NULL;
}
