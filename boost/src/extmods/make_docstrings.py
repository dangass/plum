# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Make extension module function docstrings."""

import os

os.environ["ENABLE_PLUM_BOOST"] = "NO"

from plum.data import Plum
from plum.utilities import pack, unpack


def make_docstring_lines(name: str, docstring: str):
    assert '"' not in docstring, "not supported yet"
    content = '\\n"\n"'.join(docstring.replace("\\", "\\\\").split("\n"))
    return f'\n\nPyDoc_STRVAR({name}_docstring, "{content}");'


HEADER = "/* automatically generated by make_docstrings.py */"

with open("utils_docstrings.c", "w") as fh:
    fh.write(
        "".join(
            [
                HEADER,
                make_docstring_lines("pack", pack.__doc__),
                make_docstring_lines("packmethod", Plum.pack.__func__.__doc__),
                make_docstring_lines("unpack", unpack.__doc__),
            ]
        )
    )
