# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test utilities."""

import gc
import os
import sys


MONITOR_NONE = os.environ.get("PYTHONHASHSEED", None) == "0"


class RefCounts(dict):

    """Python object reference count tracker."""

    def __init__(self, *args, **kwargs):
        # pylint: disable=super-init-not-called

        # invalid enum value exception handling cause references to be held
        # until garbage collection clears them
        gc.collect()

        # store reference counts with this offset to avoid
        # reference counts influencing other reference counts
        # because of Python's integer caching
        offset = 1000

        # always monitor common reference counts
        # FUTURE - monitor None all the time (flaky relative to PYTHONHASHSEED)
        for value in range(-7, 256):
            self[str(value)] = sys.getrefcount(value) + offset

        # monitor values passed in as arguments
        for value in args:
            self[str(value)] = sys.getrefcount(value) + offset

        # monitor values passed in as keyword arguments
        for key, value in kwargs.items():
            self[key] = sys.getrefcount(value) + offset

    def adjust(self, **kwargs):
        """Adjust reference counts.

        :param dict kwargs: adjustments
        :returns: this instance
        :rtype: RefCounts

        """
        for key, value in kwargs.items():
            self[key] += value
        return self

    def __eq__(self, other):
        actual = dict(self)
        expect = dict(other)
        result = actual == expect
        if not result:
            assert actual and expect, "did you call observe() ?"
            assert set(actual) == set(expect), "observe() keys not same"
            lines = []
            for key, before in expect.items():
                after = actual[key]
                if before > after:
                    lines.append(f"{key} = -{before - after}  (LOSS)")
                elif after > before:
                    lines.append(f"{key} = +{after - before}  (LEAK)")
            print("\n".join(lines))
        return result
