# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in boost unpack_dict() function."""

from baseline import Baseline

from plum.data import Plum, PlumMeta
from plum.exceptions import UnpackError
from plum.utilities import unpack
from plum.int import IntX

from .utils import RefCounts as RefCountsBase

uint8 = IntX(name="uint8", nbytes=1)


class UnpacksZero(Plum, metaclass=PlumMeta):  # pylint: disable=abstract-method

    """Returns zero."""

    @classmethod
    def __unpack__(cls, buffer, offset, parent, dump):
        # pylint: disable=unused-argument
        return 0, offset + 1


class CustomError(Plum, metaclass=PlumMeta):  # pylint: disable=abstract-method

    """Raises a ValueError."""

    @classmethod
    def __unpack__(cls, buffer, offset, parent, dump):
        raise ValueError("value error message")


buf0001 = b"\x00\x01"
key_a = "a"
key_b = "b"


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            uint8,
            buf0001=buf0001,
            key_a=key_a,
            key_b=key_b,
            unpack_zero_cls=UnpacksZero,
            custom_error_cls=CustomError,
        )


def test_happy_path():
    """Test unpacking with no problems.

    branches: UNPACK_DICT:WHILE:ITEM_OK

    """
    retval = key_a, key_b  # increment refs to keys (to compensate for keys returned)
    expect = RefCounts()
    retval = unpack({key_a: uint8, key_b: uint8}, buf0001)
    actual = RefCounts()

    assert retval == {key_a: 0, key_b: 1}
    assert actual == expect


def test_item_error():
    """Test unpacking with no problems.

    Start unpacking something that returns a zero instance from
    the cache (to leverage int cache reference count monitoring).
    Follow that with an unpack that fails to ensure the reference
    to the first item is thrown away.

    branches: UNPACK_DICT:WHILE:ITEM_BAD

    """
    expect_message = Baseline(
        """


        +--------+--------+-------+-------+-------------+
        | Offset | Access | Value | Bytes | Format      |
        +--------+--------+-------+-------+-------------+
        |        | ['a']  |       |       | UnpacksZero |
        |        | ['b']  |       |       | CustomError |
        +--------+--------+-------+-------+-------------+

        ValueError occurred during unpack operation:

        value error message
        """
    )

    expect = RefCounts()
    try:
        unpack({key_a: UnpacksZero, key_b: CustomError}, buf0001)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect
