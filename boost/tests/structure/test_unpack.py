# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in structure boost unpack() function."""

from baseline import Baseline

from plum.data import Plum, PlumMeta
from plum.exceptions import UnpackError
from plum.int import IntX
from plum.structure import Structure, member
from plum.utilities import getbytes, unpack

from ..utils import RefCounts as RefCountsBase

uint8 = IntX(name="uint8", nbytes=1)

buf_2_bytes = b"\x00\x01"
buf_3_bytes = b"\x00\x01\x02"


class CustomError(Plum, metaclass=PlumMeta):  # pylint: disable=abstract-method

    """Always raises ValueError when unpacked."""

    @classmethod
    def __unpack__(cls, buffer, offset, parent, dump):
        if dump:
            dump.value = "<invalid>"
            getbytes(buffer, offset, dump, 1)
        raise ValueError("invalid byte")


class Inner(Structure):

    """Sample structure (embedded in another structure)."""

    inner_a: int = member(fmt=uint8)
    inner_b: int = member(fmt=uint8)


class Outer(Structure):

    """Sample structure with embedded structure."""

    outer_a: int = member(fmt=uint8)
    outer_b: Inner = member(fmt=Inner)


class BadMember(Structure):

    """Sample structure with member that raises error when unpacked."""

    member_a: CustomError = member(fmt=CustomError)


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            uint8,
            buf_2_bytes=buf_2_bytes,
            buf_3_bytes=buf_3_bytes,
            bad_member_cls=BadMember,
            inner_cls=Inner,
            outer_cls=Outer,
        )


def test_bad_item():
    """Test exception unpacking member.

    branches: STRUCT_UNPACK:ITEM_FAIL

    """
    expect_message = Baseline(
        """


        +--------+-----------------+-----------+-------+-------------+
        | Offset | Access          | Value     | Bytes | Format      |
        +--------+-----------------+-----------+-------+-------------+
        |        |                 |           |       | BadMember   |
        | 0      | [0] (.member_a) | <invalid> | 00    | CustomError |
        +--------+-----------------+-----------+-------+-------------+

        ValueError occurred during unpack operation:

        invalid byte
        """
    )

    expect = RefCounts()
    try:
        unpack(BadMember, buf_2_bytes)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect
