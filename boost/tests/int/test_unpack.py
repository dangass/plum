# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in boost unpack() function."""

from baseline import Baseline

from plum import unpack
from plum.exceptions import UnpackError
from plum.int import IntX
from plum.enum import Enum

from ..utils import RefCounts as RefCountsBase

uint8 = IntX(name="uint8", nbytes=1)
uint16 = IntX(name="uint16", nbytes=2, byteorder="big")

buf00 = b"\x00"
buf02 = b"\x02"


class Pet(Enum, nbytes=1, strict=False):

    """Sample tolerant enumeration (not strict)."""

    CAT = 0
    DOG = 1


class StrictEnum(Enum, nbytes=1, strict=True):

    """Sample strict enumeration."""

    A = 1


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            uint8,
            uint16,
            buf00=buf00,
            buf02=buf02,
            pet_cls=Pet,
            pet_cat=Pet.CAT,
            pet_dog=Pet.DOG,
            strict_cls=StrictEnum,
            strict_a=StrictEnum.A,
        )


def test_bytes_shortage():
    """Test short a byte.

    branches: INT_UNPACK:INT_BAD

    """
    expect_message = Baseline(
        """


        +--------+----------------------+-------+--------+
        | Offset | Value                | Bytes | Format |
        +--------+----------------------+-------+--------+
        | 0      | <insufficient bytes> | 00    | uint16 |
        +--------+----------------------+-------+--------+

        InsufficientMemoryError occurred during unpack operation:

        1 too few bytes to unpack uint16 (2 needed, only 1 available)
        """
    )

    expect = RefCounts()
    try:
        unpack(uint16, buf00)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_ok_as_is():
    """Test unpack successful and no conversion to enum.

    branches: INT_UNPACK:INT_OK:KEEP_AS_INT
    branches: INT_UNPACK:GET_INT

    """
    expect = RefCounts()
    retval = unpack(uint8, buf00)
    actual = RefCounts()

    assert actual == expect
    assert retval == 0


def test_convert_ok():
    """Test unpack successful and conversion to enum successful.

    branches: INT_UNPACK:INT_OK:CONVERT:OK

    """
    retval = Pet.CAT  # this reference will be lost to compensate for returned value
    expect = RefCounts()
    retval = unpack(Pet, buf00)
    actual = RefCounts()

    assert actual == expect
    assert retval is Pet.CAT


def test_convert_fail_but_acceptable():
    """Test unpack successful and conversion to enum fails but acceptable.

    branches: INT_UNPACK:INT_OK:CONVERT:FAIL:ACCEPTABLE

    """
    expect = RefCounts()
    retval = unpack(Pet, buf02)
    actual = RefCounts()

    assert actual == expect
    assert retval == 2


def test_convert_fail_not_acceptable():
    """Test unpack successful and conversion to enum fails and not acceptable.

    branches: INT_UNPACK:INT_OK:CONVERT:FAIL:STRICT

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+------------+
        | Offset | Value | Bytes | Format     |
        +--------+-------+-------+------------+
        | 0      |       | 00    | StrictEnum |
        +--------+-------+-------+------------+

        ValueError occurred during unpack operation:

        0 is not a valid StrictEnum
        """
    )

    expect = RefCounts()
    try:
        unpack(StrictEnum, buf00)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect
