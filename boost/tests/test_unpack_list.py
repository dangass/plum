# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in boost unpack_list() function."""

from baseline import Baseline

from plum.data import Plum, PlumMeta
from plum.exceptions import UnpackError
from plum.utilities import unpack
from plum.int import IntX

from .utils import RefCounts as RefCountsBase

uint8 = IntX(name="uint8", nbytes=1)


class UnpacksZero(Plum, metaclass=PlumMeta):  # pylint: disable=abstract-method

    """Returns zero."""

    @classmethod
    def __unpack_and_dump__(cls, buffer, offset, dump):
        # pylint: disable=unused-argument
        return 0, offset + 1


class CustomError(Plum, metaclass=PlumMeta):  # pylint: disable=abstract-method

    """Raises a ValueError."""

    @classmethod
    def __unpack_and_dump__(cls, buffer, offset, dump):
        raise ValueError("value error message")


buf0001 = b"\x00\x01"


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            uint8,
            buf0001=buf0001,
            unpack_zero_cls=UnpacksZero,
            custom_error_cls=CustomError,
        )


def test_happy_path():
    """Test unpacking with no problems.

    branches: UNPACK_LIST:FOR:ITEM_OK

    """
    expect = RefCounts()
    retval = unpack([uint8, uint8], buf0001)
    actual = RefCounts()

    assert retval == [0, 1]
    assert actual == expect


def test_item_error():
    """Test unpacking with no problems.

    Start unpacking something that returns a zero instance from
    the cache (to leverage int cache reference count monitoring).
    Follow that with an unpack that fails to ensure the reference
    to the first item is thrown away.

    branches: UNPACK_LIST:FOR:ITEM_BAD

    """
    expect_message = Baseline(
        """


        +--------+--------+-------+-------+-------------+
        | Offset | Access | Value | Bytes | Format      |
        +--------+--------+-------+-------+-------------+
        |        | [0]    |       |       | UnpacksZero |
        |        | [1]    |       |       | CustomError |
        +--------+--------+-------+-------+-------------+

        ValueError occurred during unpack operation:

        value error message
        """
    )

    expect = RefCounts()
    try:
        unpack([UnpacksZero, CustomError], buf0001)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect
