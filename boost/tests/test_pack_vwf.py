# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in boost pack_value_with_format() function."""

from baseline import Baseline

from plum.exceptions import PackError
from plum.int import IntX
from plum.utilities import pack


uint8 = IntX(name="uint8", nbytes=1)


from .utils import RefCounts as RefCountsBase

key_a = "a"
key_b = "b"
key_c = "c"
fmt_dict = {key_a: uint8}
fmt_dict3 = {key_a: uint8, key_b: uint8, key_c: uint8}
fmt_empty_dict = {}
fmt_tuple = (uint8, uint8)
fmt_list = [uint8, uint8]
fmt_nested_list = (fmt_list,)
fmt_nested_dict = (fmt_dict,)
fmt_nested_plum = (uint8,)
fmt_nested_bad = (uint8, 0)
value_nested_plum = (0,)
value_nested_list = [0, 1]


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            uint8,
            fmt_dict3=fmt_dict3,
            fmt_empty_dict=fmt_empty_dict,
            fmt_nested_bad=fmt_nested_bad,
            fmt_nested_list=fmt_nested_list,
            fmt_nested_plum=fmt_nested_plum,
            fmt_list=fmt_list,
            fmt_tuple=fmt_tuple,
            key_a=key_a,
            key_b=key_b,
            key_c=key_c,
            value_nested_plum=value_nested_plum,
        )


def test_fmt_is_plum():
    """Test only one positional argument (fmt).

    branches: PACK_VWF:FMT_IS_PLUM

    """
    expect = RefCounts()
    retval = pack(value_nested_list, fmt_nested_list)
    actual = RefCounts()

    assert retval == b"\x00\x01"
    assert actual == expect


def test_fmt_is_tuple_value_is_tuple():
    """Test format is a tuple, value is tuple.

    branches: PACK_VWF:FMT_IS_SEQ:VAL_IS_SEQ

    """
    expect = RefCounts()
    retval = pack((1, 2), fmt_tuple)
    actual = RefCounts()

    assert retval == b"\x01\x02"
    assert actual == expect


def test_fmt_is_list_value_is_tuple():
    """Test format is a list.

    branches: PACK_VWF:FMT_IS_SEQ:VAL_IS_SEQ

    """
    expect = RefCounts()
    retval = pack((1, 2), fmt_list)
    actual = RefCounts()

    assert retval == b"\x01\x02"
    assert actual == expect


def test_nested_tuple_list():
    """Test format is a sequence, value is a list.

    branches: PACK_VWF:FMT_IS_SEQ:VAL_IS_SEQ

    """
    expect = RefCounts()
    retval = pack(value_nested_list, fmt_nested_list)
    actual = RefCounts()

    assert retval == b"\x00\x01"
    assert actual == expect


bad_value_len_message = Baseline(
    """


    +--------+--------+-------+-------+--------------+
    | Offset | Access | Value | Bytes | Format       |
    +--------+--------+-------+-------+--------------+
    | 0      | [0]    | 0     | 00    | uint8        |
    | 1      | [1]    | 1     | 01    | uint8        |
    +--------+--------+-------+-------+--------------+
    |        | [2]    | 2     |       | (unexpected) |
    +--------+--------+-------+-------+--------------+

    TypeError occurred during pack operation:

    3 values given, expected 2
    """
)


def test_fmt_is_tuple_value_bad_len():
    """Test format is a tuple, value len does not match.

    branches: PACK_VWF:FMT_IS_SEQ:VAL_IS_SEQ:UNEQUAL_LEN

    """
    expect = RefCounts()
    try:
        pack((0, 1, 2), fmt_tuple)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == bad_value_len_message
    assert actual == expect


def test_fmt_is_seq_value_not_seq():
    """Test format is a sequence, value is not.

    branches: PACK_VWF:FMT_IS_SEQ:VAL_IS_NOT_SEQ

    """
    exp_message = Baseline(
        """


        +--------+--------+-------+-------+---------------+
        | Offset | Access | Value | Bytes | Format        |
        +--------+--------+-------+-------+---------------+
        |        | [0]    | 0     |       | tuple or list |
        +--------+--------+-------+-------+---------------+

        TypeError occurred during pack operation:

        invalid value, expected tuple or list, got 0
        """
    )

    expect = RefCounts()
    try:
        pack(0, fmt_nested_list)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == exp_message
    assert actual == expect


def test_fmt_is_dict_value_is_dict():
    """Test format is a dict, value is a dict.

    branches: PACK_VWF:FMT_IS_DICT:VAL_IS_DICT

    """
    expect = RefCounts()
    retval = pack(dict(a=0, b=1, c=2), fmt_dict3)
    actual = RefCounts()

    assert retval == b"\x00\x01\x02"
    assert actual == expect


def test_fmt_is_dict_value_unequal_len():
    """Test format is a sequence, value is dict but has extra members.

    branches: PACK_VWF:FMT_IS_DICT:VAL_IS_DICT:UNEQUAL_LEN

    """
    exp_message = Baseline(
        """


        +--------+--------+-------+-------+--------------+
        | Offset | Access | Value | Bytes | Format       |
        +--------+--------+-------+-------+--------------+
        | 0      | ['a']  | 0     | 00    | uint8        |
        | 1      | ['b']  | 1     | 01    | uint8        |
        | 2      | ['c']  | 2     | 02    | uint8        |
        +--------+--------+-------+-------+--------------+
        |        | ['d']  | 3     |       | (unexpected) |
        +--------+--------+-------+-------+--------------+

        TypeError occurred during pack operation:

        unexpected value: 'd'
        """
    )

    expect = RefCounts()
    try:
        pack(dict(a=0, b=1, c=2, d=3), fmt_dict3)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == exp_message
    assert actual == expect


def test_fmt_is_dict_value_missing():
    """Test format is a sequence, value is dict but has missing members.

    branches: PACK_VWF:FMT_IS_DICT:VAL_IS_DICT:MISSING_KEY

    """
    exp_message = Baseline(
        """


        +--------+--------+-----------+-------+-------+
        | Offset | Access | Value     | Bytes | Format|
        +--------+--------+-----------+-------+-------+
        |        | ['a']  | (missing) |       | uint8 |
        | 0      | ['b']  | 1         | 01    | uint8 |
        |        | ['c']  | (missing) |       | uint8 |
        +--------+--------+-----------+-------+-------+

        TypeError occurred during pack operation:

        missing values: 'a', 'c'
        """
    )

    expect = RefCounts()
    try:
        pack(dict(b=1, d=3, e=4), fmt_dict3)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == exp_message
    assert actual == expect


def test_fmt_is_dict_value_not_dict():
    """Test format is a sequence, value is not a dict.

    branches: PACK_VWF:FMT_IS_DICT:VAL_NOT_DICT

    """
    exp_message = Baseline(
        """


        +--------+--------+--------+-------+------+
        | Offset | Access | Value  | Bytes | Type |
        +--------+--------+--------+-------+------+
        |        | [0]    | [0, 1] |       |      |
        +--------+--------+--------+-------+------+

        TypeError occurred during pack operation:

        invalid value, expected dict, got [0, 1]
        """
    )

    expect = RefCounts()
    try:
        pack(value_nested_list, fmt_nested_dict)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == exp_message
    assert actual == expect


def test_fmt_is_invalid():
    """Test format is invalid.

    branches: PACK_VWF:FMT_INVALID

    """
    exp_message = Baseline(
        """


        +--------+--------+-------+-------+-------------+
        | Offset | Access | Value | Bytes | Format      |
        +--------+--------+-------+-------+-------------+
        | 0      | [0]    | 0     | 00    | uint8       |
        |        | [1]    | 1     |       | 0 (invalid) |
        +--------+--------+-------+-------+-------------+

        TypeError occurred during pack operation:

        bad format, expected tuple, list, dict, or DataMeta, got 0
        """
    )

    expect = RefCounts()
    try:
        pack((0, 1), fmt_nested_bad)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == exp_message
    assert actual == expect
