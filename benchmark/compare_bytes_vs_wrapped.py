# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Compare unpacking with smart buffer or separate buffer/offset args."""

from benchmark import Measurements, PlumMeasurements

# pretty significant difference, separate args faster


def get(buffer, offset, nbytes=None):
    if nbytes:
        end = offset + nbytes
        chunk = buffer[offset:end]

        if len(chunk) != nbytes:
            raise RuntimeError()
    else:
        chunk = buffer[offset:]
        end = offset + len(chunk)

    return chunk, end


class Wrapped(bytes):
    def __init__(self, value):
        self.offset = 0

    def get(self, nbytes=None):
        if nbytes:
            start = self.offset
            self.offset += nbytes
            chunk = self[start : self.offset]

            if len(chunk) != nbytes:
                raise RuntimeError()
        else:
            chunk = self[self.offset :]
            self.offset += len(chunk)

        return chunk


class Separate(PlumMeasurements):
    def unpack(self, buffer, offset):
        chunk, offset = get(buffer, offset, 4)
        return chunk, offset

    def time_unpack(self, buffer=bytes(12)):
        items = []
        offset = 0
        item, offset = self.unpack(buffer, offset)
        items.append(item)
        item, offset = self.unpack(buffer, offset)
        items.append(item)
        item, offset = self.unpack(buffer, offset)
        items.append(item)
        if buffer[offset:]:
            raise RuntimeError()
        return items


class SeparateNoGet(PlumMeasurements):
    def unpack(self, buffer, offset):
        end = offset + 4
        chunk = buffer[offset:end]
        return chunk, end

    def time_unpack(self, buffer=bytes(12)):
        items = []
        offset = 0
        item, offset = self.unpack(buffer, offset)
        items.append(item)
        item, offset = self.unpack(buffer, offset)
        items.append(item)
        item, offset = self.unpack(buffer, offset)
        items.append(item)
        if buffer[offset:]:
            raise RuntimeError()
        return items


class Combined(PlumMeasurements):
    def unpack(self, buffer):
        chunk = buffer.get(4)
        return chunk

    def time_unpack(self, buffer=bytes(12)):
        buffer = Wrapped(buffer)
        items = [
            self.unpack(buffer),
            self.unpack(buffer),
            self.unpack(buffer),
        ]

        if buffer.get():
            raise RuntimeError()

        return items


if __name__ == "__main__":

    if PlumMeasurements.run_me(__file__):
        s = Separate()
        c = Combined()
        n = SeparateNoGet()

        PlumMeasurements.compare(s, c, n)
