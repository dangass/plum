# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Compare plum big endian and little endian integer unpacking."""

from collections import namedtuple

from benchmark import PlumMeasurements


class Dict(PlumMeasurements):
    @staticmethod
    def time_unpack(d={"a": 1, "b": 2}):
        list(d.values())


class Tuple(PlumMeasurements):
    @staticmethod
    def time_unpack(t=(1, 2)):
        list(t)


class NamedTuple(PlumMeasurements):

    nt = namedtuple("nt", "a b")

    @staticmethod
    def time_unpack(t=nt(1, 2)):
        list(t)


if __name__ == "__main__":

    if PlumMeasurements.run_me(__file__):
        d = Dict()
        t = Tuple()
        n = NamedTuple()

        PlumMeasurements.compare(d, t, n)
