# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Difference all previous measurements."""

import os

from texttable import Texttable

from benchmark import Measurements

base_dir = "measurements"

# e.g.
#   {'compare_plum_vs_struct.py': {
#       'PlumInt (boost)': {
#           '2020_04_12': {
#               "label": "PlumInt (boost)",
#               "times": {
#                   "do_nothing": 74.50580596923828,
#                   "pack": 155.3911715745926,
#                   "unpack": 152.6903361082077}}}}
db = {}

for date in os.listdir(base_dir):
    if date.startswith("__"):
        continue
    date_dir = os.path.join(base_dir, date)
    for compare in os.listdir(date_dir):
        compare_dir = os.path.join(date_dir, compare)
        for measurements in os.listdir(compare_dir):
            measurements_path = os.path.join(compare_dir, measurements)
            data = Measurements.load(measurements_path)
            db_compare = db.setdefault(compare + ".py", {})
            db_label = db_compare.setdefault(data["label"], {})
            db_label[date] = data


def print_divider_header(title):
    print("=" * 80)
    print(f" {title}.py ".center(80, "="))
    print("=" * 80)
    print()


for compare, db_compare in db.items():
    print_compare_header = True
    for label, db_label in db_compare.items():
        if len(db_label) > 1:
            if print_compare_header:
                print_compare_header = False
                print_divider_header(compare)

            row_data = {}
            for date, measurements in db_label.items():
                for method, time in measurements["times"].items():
                    if method == "do_nothing":
                        continue
                    row_data.setdefault(method, []).append(time)

            alerts = [""]
            rows = [[""] + [label.strip("_") for label in db_label]]
            for method, times in row_data.items():
                rows.append([method] + times)
                min_time = min(times)
                percentage = (max(times) - min_time) * 100 / min_time
                alerts.append("" if percentage < 6 else f"  <-- {percentage}% change")

            table = Texttable()
            num_times = len(rows[0]) - 1
            table.set_cols_dtype(["t"] + (["a"] * num_times))
            table.set_cols_align(["l"] + (["r"] * num_times))
            table.add_rows(rows)

            table_lines = table.draw().split("\n")

            for i, alert in enumerate(alerts):
                if alert:
                    table_lines[i * 2 + 1] += alert

            print("\n".join(table_lines))
            print()
            print(f" {label} ".center(len(table_lines[0]), " "))
            print()
            print()
