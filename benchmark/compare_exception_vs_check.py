# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Compare plum pack/unpack with plum vs. struct module."""

from benchmark import Measurements, PlumMeasurements

bytes_not_available = MemoryError(
    "buffer does not contain enough bytes for the request"
)


class Buffer:
    def __init__(self, offset):
        self.offset = offset
        self.buffer = bytearray(10)

    def consume_exception(self, nbytes):
        data = self.buffer[self.offset : self.offset + nbytes]

        if len(data) < nbytes:
            raise bytes_not_available

        return data

    def consume_check(self, nbytes):
        data = self.buffer[self.offset : self.offset + nbytes]

        if len(data) < nbytes:
            return None

        return data


class CatchException(PlumMeasurements):
    def time_access_success(self, buffer=Buffer(offset=0)):
        try:
            data = buffer.consume_exception(7)
        except MemoryError:
            pass
        else:
            data += b"something to do"

    def time_access_fail(self, buffer=Buffer(offset=5)):
        try:
            data = buffer.consume_exception(7)
        except MemoryError:
            pass
        else:
            data += b"something to do"


class CheckData(PlumMeasurements):
    def time_access_success(self, buffer=Buffer(offset=0)):
        data = buffer.consume_check(7)
        if data is not None:
            data += b"something to do"

    def time_access_fail(self, buffer=Buffer(offset=5)):
        data = buffer.consume_check(7)
        if data is not None:
            data += b"something to do"


if __name__ == "__main__":

    if PlumMeasurements.run_me(__file__):
        ce = CatchException()
        cd = CheckData()

        PlumMeasurements.compare(ce, cd)
