.. _install:

###################
[plum] Installation
###################

*************
Prerequisites
*************

+ `Python <https://www.python.org/>`_

    * version 3.7 or higher

+ `Requirements for Installing Packages <https://packaging.python.org/tutorials/installing-packages/#requirements-for-installing-packages>`_
  (located in the `Installing Packages <https://packaging.python.org/tutorials/installing-packages/>`_ tutorial within the
  `Python Packaging User Guide <https://packaging.python.org/>`_).


*************
Install Steps
*************

At a shell prompt, use `pip <https://pypi.python.org/pypi/pip>`_ to
automatically download and install :doc:`plum </index>`::

    python -m pip install --upgrade plum-py
