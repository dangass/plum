##################################
[plum] Tutorial: Arbitrary Nesting
##################################

.. include:: ../../alias.txt

All |plum| data store types and transforms conform to a plug and play interface.
This facilitates using any store type or transform in a place where a type or
transform embeds other types or transforms such as (but not limited to) the |ArrayX|
transform or |Structure| data store types. The plug and play architecture
facilitates practically unlimited nesting levels and combinations. The following
example demonstrates four levels of nesting. Notice in the **Access** column of
the dump that the indentation increases for every level to help visualize the nesting.

    >>> from plum.array import ArrayX
    >>> from plum.int import IntX
    >>> from plum.structure import Structure, member
    >>> from plum.utilities import unpack_and_dump
    >>>
    >>> uint8 = IntX(byteorder='little', nbytes=1, signed=False)
    >>> inner_array = ArrayX(fmt=uint8, dims=(3,))
    >>>
    >>> class MyStruct(Structure):
    ...     count: int = member(fmt=uint8)
    ...     array: list = member(fmt=inner_array)
    ...
    >>> outer_array = ArrayX(fmt=MyStruct, dims=(2,))
    >>>
    >>> x, dump = unpack_and_dump(outer_array, bytes(range(8)))
    >>> print(dump)
    +--------+---------+-------+-------+----------------------+
    | Offset | Access  | Value | Bytes | Format               |
    +--------+---------+-------+-------+----------------------+
    |        |         |       |       | List[MyStruct]       |
    |        | [0]     |       |       | MyStruct (Structure) |
    | 0      |   count | 0     | 00    | uint8                |
    |        |   array |       |       | List[int]            |
    | 1      |     [0] | 1     | 01    | uint8                |
    | 2      |     [1] | 2     | 02    | uint8                |
    | 3      |     [2] | 3     | 03    | uint8                |
    |        | [1]     |       |       | MyStruct (Structure) |
    | 4      |   count | 4     | 04    | uint8                |
    |        |   array |       |       | List[int]            |
    | 5      |     [0] | 5     | 05    | uint8                |
    | 6      |     [1] | 6     | 06    | uint8                |
    | 7      |     [2] | 7     | 07    | uint8                |
    +--------+---------+-------+-------+----------------------+
    >>>
    >>> x[1].array[1]
    6

