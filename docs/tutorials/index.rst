.. include:: ../alias.txt

################
[plum] Tutorials
################

The following tutorials supplement the reference pages.

******
Basics
******

- |Arbitrary Format|
- |Arbitrary Nesting|
- |Byte Breakdown Summaries|
- |Determining Size|
- |Packing Bytes|
- |Unpacking Bytes|


**********
Structures
**********

- |Bit Field Structure Member|
- |Dimensioned Array Structure Member|
- |Dynamically Typed Structure Member|
- |Sized Structure Member|


*************
Miscellaneous
*************

- |Customizing Structure and BitFields Methods|


.. toctree::
    :maxdepth: 2
    :hidden:

    Formats <basics/formats.rst>
    Nesting <basics/nesting.rst>
    Dumps <basics/dumps.rst>
    Size <basics/size.rst>
    Packing Bytes <basics/packing.rst>
    Unpacking Bytes <basics/unpacking.rst>

    Bit Field Member <structure/bitfields.rst>
    Dimmed Array Member <structure/dimmedarray.rst>
    Typed Member <structure/typedmember.rst>
    Sized Member <structure/sizedmember.rst>

    Customizing Methods <miscellaneous/custommethods.rst>
    