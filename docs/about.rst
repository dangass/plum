############
[plum] About
############

.. include:: alias.txt


************
Contributors
************

- Dan Gass, primary author (dan.gass@gmail.com)
- Jan Novak (job.jan.novak@gmail.com)
- Tyler Thieding (python@thieding.com)


***********
Development
***********

:Repository: https://gitlab.com/dangass/plum

.. :Documentation: :doc:`Contribution Standards, Guides, and Workflows </contribute>`


*******
License
*******

.. literalinclude:: ../LICENSE
