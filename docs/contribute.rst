.. include:: alias.txt

####################################################
[plum] Contribution Standards, Guidance and Workflow
####################################################

This page describes the coding, testing, and documentation
standards as well as the workflow that must be followed to
contribute to this project.

Within the standards, sentences with the word "SHALL" are required.
Those without "SHALL" are strongly suggested and should be followed
unless there is a very good reason to deviate.

.. contents::
    :local:


Repository: https://gitlab.com/dangass/plum

***********************************
Documentation Standard and Guidance
***********************************

These standards apply both to restructured text pages and docstrings.

* Style
    - Use active voice.

* Reference pages.
    - An entry SHALL be included for every public API.

* General
    - An example code block should demonstrate a single concept. Instead of
      covering multiple topics in a single example code block, break it up
      into multiple example code blocks (it's OK if examples build on one
      another, especially not repeating imports within a section).

    - Documentation SHALL build successfully using Sphinx (per configuration
      within the CI/CD pipeline jobs)


****************************
Coding Standard and Guidance
****************************

- `pep8` should be followed.

- All code in `src` folder shall pass `mypy`.

- Code SHALL pass PyLint (per configuration within CI/CD pipeline jobs).

    - PyLint configuration limits are at reasonable limits. For cases
      where an exception is warranted, use PyLint suppression markup
      (preferably with a comment explaining why it's warranted).


*************************************
Regression Test Standard and Guidance
*************************************

- Code SHALL pass PyTest regression tests (per configuration within CI/CD
  pipeline jobs).

    - All tests SHALL pass.

- Regression tests SHALL provide 100% code coverage (CI/CD pipelines
  verify this). Code branches deemed covered by inspection and not worth
  having regression tests for SHALL be marked with ``# pragma: no cover``.

- A single test case should focus on testing one thing (e.g. exercising
  one variation of one argument in a call).

- A group of related test cases should be organized within a test case
  class (e.g. exercising all the variations of one argument in a call).

- A test file should focus on testing one program construct (e.g. a class,
  a complex function, or a module with very simple constructs).


********************
Contributor Workflow
********************

- Restrict contribution to a single feature or anomaly fix.
- Get setup with a GitLab account: https://gitlab.com
- Get setup with Git.
- Clone the plum repository from GitLab: https://gitlab.com/dangass/plum
- Create branch off of the "master" branch.
- Read and sign the ``contribution_waiver.txt`` at the root of the repository.
- Update documentation in ``docs`` folder.

    - Add entry to ``docs/release_notes.rst`` describing the change.

        - Keep user perspective in mind.
        - Do NOT summarize code differences or implementation details.
        - If contributing an anomaly fix, describe previous behaviors and
          under what conditions it occurred (e.g. what type of exception
          happened? what specific sequence of events led to it?). Then
          describe how it behaves now (skip this if no value added).
        - List names and short descriptions of new API's (use bullets if
          multiple).

    - If changing API, update reference and tutorial pages.

- Update regression tests.

    - If contributing an anomaly fix, add a regression test that exposes
      the anomaly and tests for the correct behavior.
    - If contributing a feature, add regression tests that thoroughly
      exercises the new functionality.

- Implement the anomaly fix or new feature.

- Locally run CI/CD pipeline activities (inspect CI/CD configuration to
  discover tox commands to use).

    - Use PyLint to check your code.
    - Run the regression tests.
    - Build the documentation.

- Push branch to project GitLab server.
- Inspect associated GitLab pipeline and ensure it passed (all green).
- Create merge request.


*******************
Maintainer Workflow
*******************

- Create/check out branch.

- Edit `setup.py` and increment the revision number.

- Add entry to ``docs/release_notes.rst``.

- Commit and push branch to the central repository.

- Ensure CI/CD pipeline passes

- Get command prompt with current working directory at root of `plum` repository

- Install ``wheel`` and ``twine`` packages:

    .. code-block:: shell

        $ python -m pip install wheel twine

- Change directories into the top directory of the repository:

    .. code-block:: shell

        $ cd plum

- Remove old builds:

    .. code-block:: shell

        $ rm dist/*

- Build the `plum-py` package:

    .. code-block:: shell

        $ python setup.py bdist_wheel sdist --formats=gztar

- Upload to PyPi

    .. code-block:: shell

        $ python -m twine upload dist/*

- Alternatively, do both build and upload at same time:

    .. code-block:: shell

        $ python setup.py bdist_wheel upload

- Tag commit in GitLab with the version number.
