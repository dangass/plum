.. automodule:: plum.decimal

.. include:: ../alias.txt

###############################
[plum.decimal] Module Reference
###############################

The :mod:`plum.decimal` module provides the |DecimalX| transform for converting
an fixed point number into bytes and bytes into an fixed point number with 
given precision. This reference page demonstrates creating and using a |DecimalX|
transform as well as provides API details.

.. note::
  When packing, the given value may be rounded to fit into the given precision.

.. contents::
    :local:

The examples shown on this page require the following setup:

    >>> from plum.array import ArrayX
    >>> from plum.decimal import DecimalX
    >>> from plum.utilities import pack, unpack


*********
Basic Use
*********

The |DecimalX| transform accepts the following arguments:

    :nbytes: format size in bytes (any positive integer)
    :precision: number of digits of the fractional part
    :byteorder: ``"big"`` or ``"little"`` (default)
    :signed: ``True`` or ``False`` (default)
    :name: transform name (for representations including dump format column)

For example:

    >>> u16p1 = DecimalX(name='u16p1', nbytes=2, precision=1, byteorder='big', signed=False)

Use the transform to specify a format when using the |pack()| and |unpack()| utility
functions or when using other high level transforms:

    >>> bindata = pack(value='25.8', fmt=u16p1)
    >>> bindata.hex()
    '0102'
    >>>
    >>> unpack(fmt=u16p1, buffer=bindata)
    Decimal('25.8')


*************
API Reference
*************

.. autoclass:: DecimalX

    .. autoattribute:: byteorder

    .. autoattribute:: name

    .. autoattribute:: nbytes

    .. autoattribute:: precision

    .. autoattribute:: signed

    .. automethod:: pack

    .. automethod:: pack_and_dump

    .. automethod:: unpack

    .. automethod:: unpack_and_dump
