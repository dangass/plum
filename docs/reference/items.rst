.. automodule:: plum.items

.. include:: ../alias.txt

#############################
[plum.items] Module Reference
#############################

The :mod:`plum.items` module provides the |ItemsX| transform which converts
objects as specified by a format into bytes and vice versa. This reference
page shows how to create and use an |ItemsX| transform for packing and unpacking
bytes, a lighter weight method than a custom |Structure| subclass. This page
also provides API details.

.. contents::
    :local:

The examples shown on this page require the following setup:

    >>> from plum.array import ArrayX
    >>> from plum.littleendian import uint8
    >>> from plum.items import ItemsX
    >>> from plum.utilities import pack, unpack


***********
Basic Usage
***********

The |ItemsX| transform accepts the following arguments:

    :fmt: transform format
    :name: transform name (for representations including dump format column)

The |ItemsX| transform ``fmt`` argument accepts a transform or any dictionary,
list, or tuple of transforms (all the variations described in the
|Arbitrary Format| tutorial).

When packing, the items transform accepts values and converts them into a bytes
sequence per the ``fmt``:

    >>> fmt = ItemsX(fmt=(uint8, uint8))
    >>> buffer = pack([0, 1], fmt)
    >>> buffer
    b'\x00\x01'

When unpacking, the items transform produces values from the bytes sequence per
the ``fmt``:

    >>> unpack(fmt, buffer)
    (0, 1)

But you could just as easily pass ``[uint8, uint16]`` as the format into the
|pack()| and |unpack()| utility methods. Wrapping a format in an |ItemsX|
instance becomes necessary when specifying it as the ``fmt`` argument of
an |ArrayX| or |Structure| member. For example:

    >>> fmt = ArrayX(fmt=ItemsX(fmt=(uint8, uint8)), dims=(2,))
    >>>
    >>> buffer = pack([(0, 1), (2, 3)], fmt)
    >>> buffer
    b'\x00\x01\x02\x03'
    >>> unpack(fmt, buffer)
    [(0, 1), (2, 3)]


*************
API Reference
*************

.. autoclass:: ItemsX

    .. autoattribute:: fmt

    .. autoattribute:: name

    .. autoattribute:: nbytes

    .. automethod:: pack

    .. automethod:: pack_and_dump

    .. automethod:: unpack

    .. automethod:: unpack_and_dump