.. include:: ../alias.txt

.. automodule:: plum.optional

################################
[plum.optional] Module Reference
################################

The :mod:`plum.optional` module provides the |OptionalX| transform for
converting bytes that may or may not be present. ``None`` signifies no
bytes during conversions. This reference page demonstrates creating and
using the |OptionalX| transform with a typical use case as well as
provides API details.

.. contents::
    :local:

The examples shown on this page require the following setup:

    >>> from typing import List, Optional
    >>> from plum.array import ArrayX
    >>> from plum.int import IntX
    >>> from plum.optional import OptionalX
    >>> from plum.structure import Structure, member
    >>>
    >>> uint8 = IntX(nbytes=1, byteorder="little", signed=False)
    >>> uint16 = IntX(nbytes=2, byteorder="little", signed=False)
    >>> array = ArrayX(fmt=uint8)

*********
Basic Use
*********

The |OptionalX| transform accepts the following keyword only arguments:

    :fmt: bytes format for when bytes are present
    :name: transform name (for representations including dump format column)

One application where |OptionalX| may be useful is where the
last member of a structure may not be present. For example:

    >>> class Struct(Structure):
    ...     required_member: int = member(fmt=uint8)
    ...     optional_member: Optional[int] = member(fmt=OptionalX(fmt=uint8), default=None)
    ...
    >>> Struct.unpack(b'\x01\x02').dump()
    +--------+-----------------+-------+-------+--------------------+
    | Offset | Access          | Value | Bytes | Format             |
    +--------+-----------------+-------+-------+--------------------+
    |        |                 |       |       | Struct (Structure) |
    | 0      | required_member | 1     | 01    | uint8              |
    | 1      | optional_member | 2     | 02    | Optional[uint8]    |
    +--------+-----------------+-------+-------+--------------------+
    >>>
    >>> Struct.unpack(b'\x01').dump()
    +--------+-----------------+-------+-------+--------------------+
    | Offset | Access          | Value | Bytes | Format             |
    +--------+-----------------+-------+-------+--------------------+
    |        |                 |       |       | Struct (Structure) |
    | 0      | required_member | 1     | 01    | uint8              |
    |        | optional_member | None  |       | Optional[uint8]    |
    +--------+-----------------+-------+-------+--------------------+
    >>>
    >>> buffer, dump = Struct(required_member=1).ipack_and_dump()
    >>> buffer
    b'\x01'
    >>> print(dump)
    +--------+-----------------+-------+-------+--------------------+
    | Offset | Access          | Value | Bytes | Format             |
    +--------+-----------------+-------+-------+--------------------+
    |        |                 |       |       | Struct (Structure) |
    | 0      | required_member | 1     | 01    | uint8              |
    |        | optional_member | None  |       | Optional[uint8]    |
    +--------+-----------------+-------+-------+--------------------+
    >>>
    >>> buffer, dump = Struct(required_member=1, optional_member=2).ipack_and_dump()
    >>> buffer
    b'\x01\x02'
    >>> print(dump)
    +--------+-----------------+-------+-------+--------------------+
    | Offset | Access          | Value | Bytes | Format             |
    +--------+-----------------+-------+-------+--------------------+
    |        |                 |       |       | Struct (Structure) |
    | 0      | required_member | 1     | 01    | uint8              |
    | 1      | optional_member | 2     | 02    | Optional[uint8]    |
    +--------+-----------------+-------+-------+--------------------+

*************
API Reference
*************

.. autoclass:: OptionalX

    .. autoattribute:: fmt

    .. autoattribute:: name

    .. autoattribute:: nbytes

    .. automethod:: pack

    .. automethod:: pack_and_dump

    .. automethod:: unpack

    .. automethod:: unpack_and_dump
