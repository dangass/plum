.. include:: ../alias.txt

##################################
[plum.exceptions] Module Reference
##################################

.. automodule:: plum.exceptions

.. autoclass:: ExcessMemoryError
.. autoclass:: InsufficientMemoryError
.. autoclass:: PackError
.. autoclass:: SizeError
.. autoclass:: UnpackError
