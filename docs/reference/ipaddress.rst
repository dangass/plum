.. automodule:: plum.ipaddress

.. include:: ../alias.txt

#################################
[plum.ipaddress] Module Reference
#################################

The :mod:`plum.ipaddress` module provides transforms for converting standard
library :mod:`ipaddress` module objects into bytes and vice versa. This
reference page demonstrates creating and using these transforms as well as
provides API details.

.. contents::
    :local:

The examples shown on this page require the following setup:

    >>> from ipaddress import IPv4Address
    >>> from plum.ipaddress import IPv4AddressX
    >>> from plum.utilities import pack, unpack

************
IPv4AddressX
************

Basic Usage
===========

The |IPv4AddressX| transform accepts the following possible argument variations:

    :byteorder: ``"big"`` (default) or ``"little"``
    :name: transform name (for representations including dump format column)

For example:

    >>> ipv4address = IPv4AddressX(byteorder="little")

Use the transform to specify a format when using the |pack()| and |unpack()|  utility
functions or when using other high level transforms.

    >>> fmt = [ipv4address, ipv4address]
    >>> unpack(fmt, b'\x01\x01\xa8\xc0\x02\x01\xa8\xc0')
    [IPv4Address('192.168.1.1'), IPv4Address('192.168.1.2')]
    >>>
    >>> pack(IPv4Address('192.168.1.1'), fmt=ipv4address)
    b'\x01\x01\xa8\xc0'

The transform also accepts integer values when packing:

    >>> pack(3232235777, fmt=ipv4address)
    b'\x01\x01\xa8\xc0'

.. Tip::
    Use ``sys.byteorder`` as the |IPv4AddressX| transform ``byteorder`` argument to
    get the same byte order as the architecture of the machine your script
    is running on.

API Reference
=============

.. autoclass:: IPv4AddressX

    .. autoattribute:: byteorder

    .. autoattribute:: nbytes

    .. automethod:: pack

    .. automethod:: pack_and_dump

    .. automethod:: unpack

    .. automethod:: unpack_and_dump
