.. automodule:: plum.bigendian

.. include:: ../alias.txt

#################################
[plum.bigendian] Module Reference
#################################

The :mod:`plum.bigendian` module provides the following big endian byte order
transforms for your convenience.

    ==============================  ================================================
    Transform                       Description
    ==============================  ================================================
    double                          double precision 64-bit float
    single                          single precision 64-bit float
    sint8                           signed 8-bit integer
    sint16                          signed 16-bit integer
    sint32                          signed 32-bit integer
    sint64                          signed 64-bit integer
    uint8                           unsigned 8-bit integer
    uint16                          unsigned 16-bit integer
    uint32                          unsigned 32-bit integer
    uint64                          unsigned 64-bit integer
    ==============================  ================================================
