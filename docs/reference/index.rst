####################
[plum] API Reference
####################

.. include:: ../alias.txt

.. currentmodule:: plum

***********************
Data Types / Transforms
***********************

    ==============================  ================================================
    Module                          Description
    ==============================  ================================================
    :mod:`plum.array`               list of uniformly typed
    :mod:`plum.attrdict`            dictionary of uniquely typed items
    :mod:`plum.bigendian`           common transforms in big endian byte order
    :mod:`plum.bitfields`           integer with bit field accessors
    :mod:`plum.bytes`               bytes sequence
    :mod:`plum.decimal`             fixed point decimal numbers
    :mod:`plum.enum`                integer enumerated constants
    :mod:`plum.flag`                integer with bit flags
    :mod:`plum.float`               floating point
    :mod:`plum.int`                 integer
    :mod:`plum.ipaddress`           IP address/network/interface objects
    :mod:`plum.items`               collection of uniquely typed items
    :mod:`plum.littleendian`        common transforms in little endian byte order
    :mod:`plum.none`                no bytes
    :mod:`plum.optional`            optional item
    :mod:`plum.sized`               variably sized object with size header
    :mod:`plum.str`                 string
    :mod:`plum.structure`           predefined structure of uniquely typed members
    ==============================  ================================================

.. toctree::
    :hidden:

    array <array.rst>
    attrdict <attrdict.rst>
    bigendian <bigendian.rst>
    bitfields <bitfields.rst>
    buffer <buffer.rst>
    bytes <bytes.rst>
    decimal <decimal.rst>
    float <float.rst>
    exceptions <exceptions.rst>
    enum <enum.rst>
    flag <flag.rst>
    int <int.rst>
    ipaddress <ipaddress.rst>
    items <items.rst>
    littleendian <littleendian.rst>
    none <none.rst>
    optional <optional.rst>
    sized <sized.rst>
    str <str.rst>
    structure <structure.rst>
    utilities <utilities.rst>

*************************
Utility Classes/Functions
*************************

    ===================================  ================================================================================
    Class/Function                       Description
    ===================================  ================================================================================
    |Buffer|                             Bytes sequence incremental unpacker.
    |getbytes()|                         Get bytes from bytes buffer.
    |pack()|                             Pack values as bytes following a format.
    |pack_and_dump()|                    Pack values as bytes and produce bytes summary following a format.
    |unpack()|                           Unpack item(s) from bytes.
    |unpack_and_dump()|                  Unpack item(s) from bytes and produce packed bytes summary.
    ===================================  ================================================================================

.. toctree::
    :hidden:

    buffer <buffer.rst>
    utilities <utilities.rst>

**********
Exceptions
**********

    - |ExcessMemoryError|
    - |InsufficientMemoryError|
    - |PackError|
    - |SizeError|
    - |UnpackError|


.. toctree::
    :hidden:

    exceptions <exceptions.rst>

